# crypto_panel

Used in conjunction with `xfce4-genmon-plugin` to get desired crypto prices on my panel.

Get your API key here:

[https://coinmarketcap.com/api](https://coinmarketcap.com/api)
