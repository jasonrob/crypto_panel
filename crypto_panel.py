#!/usr/bin/env python3

import requests
import json
import re

api_key = ''
coin_url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest'

query = {
    'symbol': 'BTC,ETH,XMR,ARRR,DERO,RTM'
}

headers = {
  'Accepts': 'application/json',
  'X-CMC_PRO_API_KEY': api_key
}

resp = requests.get(coin_url, params=query, headers=headers)
data = resp.json()

prices = '<txt>'
for coin in data['data']:
    price = float(data['data'][coin]['quote']['USD']['price'])
    change_hour = data['data'][coin]['quote']['USD']['percent_change_1h']
    if change_hour > 0:
      prices += '<span foreground="#A3BE8C">{} {:.2f}</span> | '.format(coin,price)
    else:
      prices += '<span foreground="#BF616A">{} {:.2f}</span> | '.format(coin,price)

prices = re.sub(r'\|\s$','',prices)
prices += '</txt>'
print(prices)
